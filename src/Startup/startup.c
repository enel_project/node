
#include "../Drivers/perepherals.h"

#if !defined  (HSE_VALUE) 
  #define HSE_VALUE    ((uint32_t)8000000U) 
#endif 

#if !defined  (MSI_VALUE)
  #define MSI_VALUE    ((uint32_t)2000000U)
#endif 
   
#if !defined  (HSI_VALUE)
  #define HSI_VALUE    ((uint32_t)16000000U) 
#endif 


// #define VECT_TAB_SRAM 00u
#define VECT_TAB_OFFSET  0x00U // Offset, multiply by 0x200
                                   


uint32_t SystemCoreClock = 2000000U;
//const uint8_t AHBPrescTable[16] = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 1U, 2U, 3U, 4U, 6U, 7U, 8U, 9U};
//const uint8_t APBPrescTable[8] = {0U, 0U, 0U, 0U, 1U, 2U, 3U, 4U};
//const uint8_t PLLMulTable[9] = {3U, 4U, 6U, 8U, 12U, 16U, 24U, 32U, 48U};


using namespace driver::RCCDriver;

static const uint32_t FLASH_BASE = 0x08000000;
static const uint32_t VECTOR_OFFSET = 0x0;	

extern "C" {
	void SystemInit (void) {    

		SCB->VTOR = FLASH_BASE | VECTOR_OFFSET; 
		
		RCCDriver rcc;
		
		rcc.open(HSI16);
		//rcc.open(HSI48);
		//rcc.open(HSE);
		rcc.open(LSI);
		//rcc.open(LSE);

		rcc.sysclk(HSI16);
	
	}
}

// EOF
