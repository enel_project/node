
#include "registers.h"
#include "usart_driver.h"
#include <stdint.h>

namespace driver {
	USARTDriver::USARTDriver() : Driver() {}
	USARTDriver::~USARTDriver() {}
			
	void USARTDriver::open() {}
	void USARTDriver::close() {}
	void USARTDriver::start() {}
	void USARTDriver::stop() {}
}

// EOF
