
#include "registers.h"
#include <stdint.h>

#ifndef HEADER_DRIVER_HEADER
#define HEADER_DRIVER_HEADER

namespace driver {
	class Driver {
		
		protected:
		bool driver_state;
		
		// Protected Methods
		protected:
		Driver() { driver_state = false; }
		~Driver() {}
		// Public Methods
		public:
		
		virtual bool get_state() { return driver_state; }
		virtual void set_state(bool state) { this->driver_state = state; }
			
	};
}

#endif

// EOF
