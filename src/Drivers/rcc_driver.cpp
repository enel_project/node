
#include "registers.h"
#include "rcc_driver.h"
#include <stdint.h>

static const uint32_t RCC_HSI_READY = (1u << 2);
static const uint32_t RCC_MSI_READY = (1u << 9);
static const uint32_t RCC_HSE_READY = (1u << 17);
static const uint32_t RCC_PLL_READY = (1u << 25);
static const uint32_t RCC_LSI_READY = (1u << 1);
static const uint32_t RCC_LSE_READY = (1u << 9);

namespace driver {
	namespace RCCDriver {
		RCCDriver::RCCDriver() : Driver() {}
		RCCDriver::~RCCDriver() {}
			
		void RCCDriver::open(clock_t clock) {
			switch (clock) {
				case HSI16:
					RCC->CR |= (1u << 0);
					while (!(RCC->CR & RCC_HSI_READY));
					break;
				case HSI48:
					RCC->CRRCR |= (1u << 0);
				case HSE:
					RCC->CR |= (1u << 16);
					while (!(RCC->CR & RCC_HSE_READY));
					break;
				case MSI:
					RCC->CR |= (1u << 8);
					while (!(RCC->CR & RCC_MSI_READY));
					break;
				case PLL:
					RCC->CR |= (1u << 24);
					while (!(RCC->CR & RCC_PLL_READY));
					break;
				case LSI:
					PWR->CR |= (1u << 8);
					RCC->CSR |= (1u << 0);
					while (!(RCC->CSR & RCC_LSI_READY));
					PWR->CR &= ~(1u << 8);
					break;
				case LSE:
					PWR->CR |= (1u << 8);
					RCC->CSR |= (1u << 8);
					while (!(RCC->CSR & RCC_LSE_READY));
					PWR->CR &= ~(1u << 8);
					break;
				default:
					break; 
			}
		}
		void RCCDriver::close() {
			RCC->CR &= 		0x00808081;
			RCC->CRRCR &= 0x00000001;
		}
		void RCCDriver::close(clock_t clock) {
			switch (clock) {
				case HSI16:
					RCC->CR &= ~(1u << 0);
					break;
				case HSI48:
					RCC->CRRCR &= ~(1u << 0);
				case HSE:
					RCC->CR &= ~(1u << 16);
					break;
				case MSI:
					RCC->CR &= ~(1u << 8);
					break;
				case PLL:
					RCC->CR &= ~(1u << 24);
					break;
				case LSI:
					break;
				case LSE:
					break;
				default:
					break; 
			}
		}
		void RCCDriver::sysclk(clock_t clock) {
			RCC->CFGR &= ~(0x3 << 0);
			RCC->CFGR |= (clock && 0x3);
		}
		void RCCDriver::pll(clock_t clock, pllMul_t mul, pllDiv_t div) {
			switch (clock) {
				case HSI16:
					RCC->CFGR &= ~(1u << 16);
					RCC->CFGR &= ~(0xF << 18);
					RCC->CFGR |= (mul << 18);
					RCC->CFGR &= ~(0x3 << 22);
					RCC->CFGR |= (div << 22);
					break;
				case HSE:
					RCC->CFGR |= (1u << 16);
					RCC->CFGR &= ~(0xF << 18);
					RCC->CFGR |= (mul << 18);
					RCC->CFGR &= ~(0x3 << 22);
					RCC->CFGR |= (div << 22);
					break;
				default:
					break;
			}
		}
		void RCCDriver::start(perepheral_t pereph) {
			switch (pereph) {
				case IOPA:
					RCC->IOPENR |= (1u << 0);
					break;
				case IOPB:
					RCC->IOPENR |= (1u << 1);
					break;
				case IOPC:
					RCC->IOPENR |= (1u << 2);
					break;
				case IOPD:
					RCC->IOPENR |= (1u << 3);
					break;
				case IOPE:
					RCC->IOPENR |= (1u << 4);
					break;
				case IOPH:
					RCC->IOPENR |= (1u << 7);
					break;
				case NVMP:
					RCC->AHBENR |= (1u << 8);
					break;
				case DMAP:
					RCC->AHBENR |= (1u << 0);
					break;
				case USARTP:
					RCC->APB2ENR |= (1u << 14);
					break;
				case ADCP:
					RCC->APB2ENR |= (1u << 9);
					break;
                case SYSCFGEN:
                    RCC->APB2ENR |= (1u << 0);
                    break;
				case LPTIMP:
                    RCC->CCIPR |= (1u << 18);
					RCC->APB1ENR |= (1u << 31);
					break;
				case PWRP:
					RCC->APB1ENR |= (1u << 28);
					break;
				case LPUARTP:
                    RCC->CCIPR |= (2u << 10);
					RCC->APB1ENR |= (1u << 18);
					break;
				case TIM21P:
					RCC->APB2ENR |= (1u << 2);
					break;
				case TIM2P:
					RCC->APB1ENR |= (1u << 0);
					break;
				default:
					break;
			}
		}
		void RCCDriver::stop() {
			RCC->IOPENR =  0x00000000;
			RCC->AHBENR =  0x00000100;
			RCC->APB2ENR = 0x00000000;
			RCC->APB1ENR = 0x00000000;
		}
		void RCCDriver::stop(perepheral_t pereph) {
		switch (pereph) {
				case IOPA:
					RCC->IOPENR &= ~(1u << 0);
					break;
				case IOPB:
					RCC->IOPENR &= ~(1u << 1);
					break;
				case IOPC:
					RCC->IOPENR &= ~(1u << 2);
					break;
				case IOPD:
					RCC->IOPENR &= ~(1u << 3);
					break;
				case IOPE:
					RCC->IOPENR &= ~(1u << 4);
					break;
				case IOPH:
					RCC->IOPENR &= ~(1u << 7);
					break;
				case NVMP:
					RCC->AHBENR &= ~(1u << 8);
					break;
				case DMAP:
					RCC->AHBENR &= ~(1u << 0);
					break;
				case USARTP:
					RCC->APB2ENR &= ~(1u << 14);
					break;
				case ADCP:
					RCC->APB2ENR &= ~(1u << 9);
					break;
                case SYSCFGEN:
                    RCC->APB2ENR &= ~(1u << 0);
                    break;
				case LPTIMP:
					RCC->APB1ENR &= ~(1u << 31);
					break;
				case PWRP:
					RCC->APB1ENR &= ~(1u << 28);
					break;
				case LPUARTP:
					RCC->APB1ENR &= ~(1u << 18);
					break;
				case TIM21P:
					RCC->APB2ENR &= ~(1u << 2);
					break;
				case TIM2P:
					RCC->APB1ENR &= ~(1u << 0);
					break;
				default:
					break;
			}
		}
	}
}	

// EOF
