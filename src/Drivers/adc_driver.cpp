
#include "registers.h"
#include "rcc_driver.h"
#include "gpio_driver.h"
#include "dma_driver.h"
#include "adc_driver.h"
#include <stdint.h>

static const uint32_t ADC_CALIBRATION_PENDING = (1u << 31);
static const uint32_t ADC_CONVERSION_PENDING = (1u << 2);
static const uint32_t ADC_READY_FLAG = (1u << 0);

using driver::GPIODriver::PORTA;
using driver::GPIODriver::ANALOG;
using driver::GPIODriver::PUSHPULL;
using driver::GPIODriver::HIGH;
using driver::GPIODriver::PULLDOWN;
using driver::RCCDriver::IOPA;
using driver::RCCDriver::ADCP;

namespace driver {
    namespace ADCDriver {
        typedef enum {
            ADC_INIT,
            ADC_IDLE, 
            ADC_START,
            ADC_CHANNEL_0, 
            ADC_CHANNEL_1, 
            ADC_CHANNEL_2,
            ADC_READY,
            ADC_OFF,
            ADC_ERROR 
        } adc_state_t;
    }
}

static driver::ADCDriver::adc_state_t adc_state;
volatile static uint16_t channel_0;
volatile static uint16_t channel_1;
volatile static uint16_t channel_2;

namespace driver {
    namespace ADCDriver {

        ADCDriver::ADCDriver() : Driver(), rcc(), gpioa(PORTA) {
            // *1* Configure ADC state machine to init
            // *2* Initialize adc variables to zero
            adc_state = ADC_INIT;   // *1*
            channel_0 = 0;          // *2*
            channel_1 = 0;          // *2*
            channel_2 = 0;          // *2*
        }
        ADCDriver::~ADCDriver() { this->close(); }
                
        void ADCDriver::open() {
            // *1*  Start ADC and GPIOA clocks
            // *2*  Configure PA0, PA1, and PA2 as analog input
            // *3*  Start ADC calibration
            // *4*  Configure ADC in discontinuous mode
            // *5*  Set sampling time (160.5 cycles -> 111)
            // *6*  Select channels to convert
            // *7*  Unmask interrupt in NVIC
            // *8*  Enable EOC interrupt
            // *9*  Enable ready interrupt
            // *10* Configure adc state to idle
            
            rcc.start(ADCP);            // *1*
            rcc.start(IOPA);            // *1*
                                        // *2*
            gpioa.open(0, ANALOG, PUSHPULL, HIGH, PULLDOWN);
            gpioa.open(1, ANALOG, PUSHPULL, HIGH, PULLDOWN);
            gpioa.open(2, ANALOG, PUSHPULL, HIGH, PULLDOWN);
            
            ADC1->CR |= (1u << 31);     // *3*
                                        // *3*
            while (ADC1->CR & ADC_CALIBRATION_PENDING);
            ADC1->CFGR1 |= (1u << 16);  // *4*
            ADC1->SMPR &= ~(7u << 0);   // *5*
            ADC1->SMPR |= (7u << 0);    // *5*
            ADC1->CHSELR = 0x00000000;  // *6*
            ADC1->CHSELR |= (1u << 0) |
                            (1u << 1) |
                            (1u << 2);  // *6*
            NVIC->ISER |= (1u << 12);   // *7*
            ADC1->IER |= (1u << 2);     // *8*
            ADC1->IER |= (1u << 0);     // *9*
            adc_state = ADC_IDLE;       // *10*
        }
        void ADCDriver::close() {
            // *1* Set all ADC registers to default
            // *2* reset GPIO pins to default
            // *3* Turn off ADC clock
            // *4* Configure adc state to init
            
            ADC1->CR =      0x00000000; // *1*
            ADC1->CFGR1 =   0x00000000; // *1*
            ADC1->SMPR =    0x00000000; // *1*
            ADC1->CHSELR =  0x00000000; // *1*
            
            gpioa.close(0);             // *2*
            gpioa.close(1);             // *2*
            gpioa.close(2);             // *2*
            
            rcc.stop(ADCP);             // *3*
            
            adc_state = ADC_INIT;       // *4*
        }
        void ADCDriver::start() {
            // *1* Start ADC and GPIOA clocks
            // *2* Enable ADC
            // *3* Configure adc state to start
            rcc.start(ADCP);            // *1*
            rcc.start(IOPA);            // *1*
            ADC1->CR |= (1u << 0);      // *2*
            adc_state = ADC_START;      // *3*
        }
        void ADCDriver::stop() {
            // *1* Wait for conversion to finish
            // *2* Disable ADC
            // *3* Turn off ADC clock
            // *4* Configure adc state to off
                                        // *1*
            while (ADC1->CR & ADC_CONVERSION_PENDING);
            ADC1->CR |= (1u << 1);      // *2*
            rcc.stop(ADCP);             // *3*
            adc_state = ADC_OFF;        // *4*
        }
        void ADCDriver::calibrate() {
            // *1* Start calibration
            // *2* Wait for calibration to finish
            
            ADC1->CR |= (1u << 31);     // *1*
                                        // *2*
            while (ADC1->CR & ADC_CALIBRATION_PENDING);
        }
        bool ADCDriver::read_ready() {
            // *1* Return true if adc is ready to read
            return (adc_state == ADC_READY) ? true : false;
        }
        uint16_t ADCDriver::read(uint8_t channel) {
            // *1* Ensure channel is not out of bounds
            // *2* Return adc channel data
            if (channel > 2) assert(false); // *1*
                                            // *2*    
            return ((channel == 0) ? channel_0 : ((channel == 1) ? channel_1 : ((channel == 2) ? channel_2 : 0)));
        }
    }
}
	
using driver::ADCDriver::ADC_CHANNEL_0;
using driver::ADCDriver::ADC_CHANNEL_1;
using driver::ADCDriver::ADC_CHANNEL_2;
using driver::ADCDriver::ADC_READY;

void ADC1_CONVERSION_IRQ_Handler() {
    // *1* Read channel
    // *2* Start next sequence
    // *3* Transition state machine
    
    switch (adc_state) {
        case ADC_CHANNEL_0:
            channel_0 = ADC1->DR;       // *1*
            ADC1->CR |= (1u << 2);      // *2*
            adc_state = ADC_CHANNEL_1;  // *3*
            break;
        case ADC_CHANNEL_1:
            channel_1 = ADC1->DR;       // *1*
            ADC1->CR |= (1u << 2);      // *2*
            adc_state = ADC_CHANNEL_2;  // *3*
            break;
        case ADC_CHANNEL_2:
            channel_2 = ADC1->DR;       // *1*
            adc_state = ADC_READY;      // *3*
            break;
        default:
            assert(false);
            break;
    }
    GPIOA->ODR ^= (1u << 5);
}

void ADC1_READY_IRQ_Handler() {
    // *1* Start ADC conversions
    // *2* Transition state machine to channel 0
    ADC1->CR |= (1u << 2);              // *1*
    adc_state = ADC_CHANNEL_0;          // *2*
}

extern "C" {
    void ADC1_COMP_IRQHandler(void) {
        // *1* ADC conversion finished handler
        // *2* ADC ready to start handler
        if (ADC1->ISR & ADC_CONVERSION_PENDING) {
            ADC1_CONVERSION_IRQ_Handler();  // *1*
        } else if (ADC1->ISR & ADC_READY_FLAG) {
            ADC1->ISR &= ~(1u << ADC_READY_FLAG);
            ADC1_READY_IRQ_Handler();       // *2*
        } else {}
    }
}

// EOF
