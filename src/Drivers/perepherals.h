
#ifndef HEADER_PEREPHERALS_HEADER
#define HEADER_PEREPHERALS_HEADER

#include "adc_driver.h"
#include "gpio_driver.h"
#include "lptim_driver.h"
#include "lpuart_driver.h"
#include "rcc_driver.h"
#include "usart_driver.h"
#include "tim_driver.h"
#include "dma_driver.h"
#include "pwr_driver.h"
#include "nvm_driver.h"
#include "nvic_driver.h"

#endif

// EOF
