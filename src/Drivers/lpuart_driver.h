
#include "registers.h"
#include "gpio_driver.h"
#include "rcc_driver.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_LPUARTDRIVER_HEADER
#define HEADER_LPUARTDRIVER_HEADER

namespace driver {
	namespace LPUARTDriver {
		class LPUARTDriver : public Driver {
			
			public:
			LPUARTDriver();
			~LPUARTDriver();
			
			virtual void open();
			virtual void close();
			virtual void start();
			virtual void stop();
			unsigned char read();
			void write(unsigned char message);
			bool read_ready();
			bool write_ready();
			uint8_t get_length();
			void clear_buffer();
			
			private:
			
			RCCDriver::RCCDriver rcc;
			driver::GPIODriver::GPIODriver gpiob;
			
		};
	}
}

#endif

// EOF
