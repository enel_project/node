
#include "registers.h"
#include "nvm_driver.h"
#include <stdint.h>

namespace driver {
    namespace NVMDriver {
        NVMDriver::NVMDriver() : Driver() {}
        NVMDriver::~NVMDriver() {}
                
        void NVMDriver::open() {
            // *1* Unlock EEPROM
            NVM->PEKEYR = 0x89ABCDEF;
            NVM->PEKEYR = 0x02030405;
        }
        void NVMDriver::close() {
            // *1* Lock EEPROM
            NVM->PECR |= (1u << 0);
        }
        void NVMDriver::start() {
        
            // test
            //EEPROM->voltage[0] = 0x0101;
            //EEPROM->voltage[1] = 0xF1F1;
            EEPROM->size = 1;
        }
        void NVMDriver::stop() {}
        
            
    }
}

// EOF
