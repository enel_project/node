
#include "registers.h"
#include "dma_driver.h"
#include <stdint.h>

namespace driver {
    namespace DMADriver {
        DMADriver::DMADriver() : Driver() {}
        DMADriver::~DMADriver() {}
                
        void DMADriver::open() {}
        void DMADriver::close() {}
        void DMADriver::start() {}
        void DMADriver::stop() {}
    }
}

// EOF
