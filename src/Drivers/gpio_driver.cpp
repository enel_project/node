
#include "registers.h"
#include "gpio_driver.h"
#include <stdint.h>

static void (*EXTI0Handler)();
static void (*EXTI1Handler)();
static void (*EXTI2Handler)();

static void empty_handler() {}

namespace driver {
	namespace GPIODriver {
		GPIODriver::GPIODriver(port_t port) : Driver() {
			static GPIO_type *PORT_VALS[6] = { GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOH };
			this->GPIO = PORT_VALS[port];
			this->port = port;
		}
		GPIODriver::~GPIODriver() {}
				
		void GPIODriver::open(pin_t pin, mode_t mode, type_t type, speed_t speed, pull_t pull, afio_t afio) {
			
            switch (port) {
                case PORTA:
                    rcc.start(RCCDriver::IOPA);
                    break;
                case PORTB:
                    rcc.start(RCCDriver::IOPB);
                    break;
                case PORTC:
                    rcc.start(RCCDriver::IOPC);
                    break;
                case PORTD:
                    rcc.start(RCCDriver::IOPD);
                    break;
                case PORTH:
                    rcc.start(RCCDriver::IOPH);
                    break;
                default:
                    assert(false);
                    break;
            }
			// Reset pin states
			GPIO->MODER &= ~(0x3 << (2 * pin));
			GPIO->OSPEEDR &= ~(0x3 << (2 * pin));
			GPIO->PUPDR &= ~(0x3 << (2 * pin));
			// Set pin states
			GPIO->MODER |= (mode << (2 * pin));
			GPIO->OTYPER |= (type << pin);
			GPIO->OSPEEDR |= (speed << (2 * pin));
			GPIO->PUPDR |= (pull << (2 * pin));
			
			// Set AFIO number if applicable
			if (mode == ALTERNATE) {
				if (pin < 8) {
					GPIO->AFRL &= ~(0xf << (4 * pin));
					GPIO->AFRL |= (afio << (4 * pin));
				} else {
					GPIO->AFRL &= ~(0xf << (4 * (pin - 8)));
					GPIO->AFRH |= (afio << (4 * (pin - 8)));
				}
			}
		}
		void GPIODriver::close() {
			// Set everything to reset state
			// Reset state is dependant on specific port
			if (this->port == PORTA) {
				GPIO->MODER = 0xEBFFFCFF;
				GPIO->OSPEEDR = 0x0C000000;
				GPIO->PUPDR = 0x24000000;
			} else {
				GPIO->MODER = 0xFFFFFFFF;
				GPIO->OSPEEDR = 0x00000000;
				GPIO->PUPDR = 0x00000000;
			}
			GPIO->OTYPER = 0x00000000;
			GPIO->AFRH = 0x00000000;
			GPIO->AFRL = 0x00000000;
		}

		void GPIODriver::close(pin_t pin) {
			// Reset pin states to zero
			GPIO->MODER &= ~(0xF << (2 * pin));
			GPIO->OTYPER &= ~(0x1 << pin);
			GPIO->OSPEEDR &= ~(0xF << (2 * pin));
			GPIO->PUPDR &= ~(0xF << (2 * pin));
			// Set pins to default
			if (this->port == PORTA) {
				GPIO->MODER |= (0xEBFFFCFF && (0xF << (2 * pin)));
				GPIO->OSPEEDR |= (0x0C000000 && (0xF << (2 * pin)));
				GPIO->PUPDR |= (0x24000000 && (0xF << (2 * pin)));
			} else {
				GPIO->MODER |= (0xFFFFFFFF && (0xF << (2 * pin)));
			}
			// Reset AFIO registers to zero
			if (pin < 8) {
				GPIO->AFRL &= ~(0xFF << (4 * pin));
			} else {
				GPIO->AFRH &= ~(0xFF << (4 * pin));
			}
		}

		void GPIODriver::start() { static_assert(true, "GPIODriver::start() is not implemented"); } // Unused
		void GPIODriver::stop() { static_assert(true, "GPIODriver::stop() is not implemented"); }  // Unused
			
		void GPIODriver::set(pin_t pin) {
			GPIO->BSRR |= (1u << pin);
		}
		void GPIODriver::rset(pin_t pin) {
			GPIO->BRR |= (1u << pin);
		}
		void GPIODriver::toggle(pin_t pin) {
			GPIO->BSRR |= (GPIO->ODR & (1u << pin)) ? (1u << (pin + 16)) : (1u << pin);		
		}
        bool GPIODriver::read(pin_t pin) {
            return (GPIO->IDR & (1u << pin));
        }
        void GPIODriver::unmask(pin_t pin, void (*EXTIxHandler)()) {
            // *1* Enable SYSCFG clock
            // *2* Unmask NVIC interrupt
            // *3* Enable EXTI interrupt
            // *4* Assign gpio port
            // *5* Enable rising edge
            // *6* Assign callback function
            // *7* Turn SYSCFG clock off
            rcc.start(RCCDriver::SYSCFGEN);         // *1*
            if (pin < 2) {
                NVIC->ISER |= (1u << 5);            // *2*
                EXTI->IMR |= (1u << pin);           // *3*
                SYSCFG->EXTICR1 |= (port << (pin * 4));   // *4*
                EXTI->RTSR |= (1u << pin);          // *5*
                EXTI0Handler = EXTIxHandler;        // *6*
            } else if (pin < 4) {
                NVIC->ISER |= (1u << 6);            // *2*
                EXTI->IMR |= (1u << pin);           // *3*
                SYSCFG->EXTICR1 |= (port << (pin * 4));   // *4*
                EXTI->RTSR |= (1u << pin);          // *5*
                EXTI1Handler = EXTIxHandler;        // *6*
            } else if (pin < 8) {
                NVIC->ISER |= (1u << 7);            // *2*
                EXTI->IMR |= (1u << pin);           // *3*
                SYSCFG->EXTICR2 |= (port << (pin * 4));   // *4*
                EXTI->RTSR |= (1u << pin);          // *5*
                EXTI2Handler = EXTIxHandler;        // *6*
            } else if (pin < 12) {
                NVIC->ISER |= (1u << 7);            // *2*
                EXTI->IMR |= (1u << pin);           // *3*
                SYSCFG->EXTICR3 |= (port << (pin * 4));   // *4*
                EXTI->RTSR |= (1u << pin);          // *5*
                EXTI2Handler = EXTIxHandler;        // *6*
            } else if (pin < 16) {
                NVIC->ISER |= (1u << 7);            // *2*
                EXTI->IMR |= (1u << pin);           // *3*
                SYSCFG->EXTICR4 |= (port << (pin * 4));   // *4*
                EXTI->RTSR |= (1u << pin);          // *5*
                EXTI2Handler = EXTIxHandler;        // *6*
            } else {
                return;
            }
            rcc.stop(RCCDriver::SYSCFGEN);          // *7*
            
        }
        void GPIODriver::mask(pin_t pin) {
            // *1* Mask NVIC interrupt
            // *2* Disable EXTI interrupt
            // *3* Clear interrupt handlers
            if (pin < 2) {
                //NVIC->ICER |= (1u << 5);            // *1*
                EXTI->IMR &= ~(1u << pin);          // *2*
                EXTI0Handler = empty_handler;       // *3*
            } else if (pin < 4) {
                NVIC->ICER |= (1u << 6);            // *1*
                EXTI->IMR &= ~(1u << pin);          // *2*
                EXTI1Handler = empty_handler;       // *3*
            } else if (pin < 16) {
                NVIC->ICER |= (1u << 7);            // *1*
                EXTI->IMR &= ~(1u << pin);          // *2*
                EXTI2Handler = empty_handler;       // *3*
            } else {
                return;
            }
        }
	}
}

extern "C" {
    void EXTI0_1_IRQHandler(void) { 
        (*EXTI0Handler)(); 
        EXTI->PR |= (3u << 0);
    }
    void EXTI2_3_IRQHandler(void) { 
        (*EXTI1Handler)(); 
        EXTI->PR |= (3u << 2);
    }
    void EXTI4_15_IRQHandler(void) { 
        (*EXTI2Handler)(); 
        EXTI->PR |= (0xFFF << 4);
    }
}

// EOF
