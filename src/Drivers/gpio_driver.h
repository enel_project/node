
#include "registers.h"
#include "driver.h"
#include "rcc_driver.h"
#include <stdint.h>

#ifndef HEADER_GPIODRIVER_HEADER
#define HEADER_GPIODRIVER_HEADER

namespace driver {
	namespace GPIODriver {
		typedef enum {
			PORTA=0, PORTB=1, PORTC=2, PORTD=3, PORTE=4, PORTH=5
		} port_t;
			
		typedef uint8_t pin_t;
        typedef uint8_t exti_t;
			
		typedef enum {
			INPUT, OUTPUT, ALTERNATE, ANALOG
		} mode_t;

		typedef enum {
			PUSHPULL, OPENDRAIN
		} type_t;
			
		typedef enum {
			LOW, MED, HIGH, VHIGH
		} speed_t;
			
		typedef enum {
			NONE, PULLUP, PULLDOWN
		} pull_t;
			
		typedef enum {
			AFIO0, AFIO1, AFIO2, AFIO3, AFIO4, AFIO5, AFIO6, AFIO7
		} afio_t;
			
		class GPIODriver : public Driver {
			
			public:
						
			GPIODriver(port_t);
			~GPIODriver();
			
			void open(pin_t, mode_t, type_t, speed_t, pull_t, afio_t = AFIO0);
			void close();
			void close(pin_t);
			void start();
			void stop();
			void set(pin_t);
			void rset(pin_t);
			void toggle(pin_t);
			bool read(pin_t);
            void unmask(pin_t, void (*)());
            void mask(pin_t);
            
			private:
			port_t port;
            RCCDriver::RCCDriver rcc;
			volatile GPIO_type *GPIO;
			
		};
	}
}
#endif

// EOF
