/**
 *  Title: 
 *    registers.h
 *  Project: 
 *    Lab 3
 *  Course:
 *    ENEL 487
 *  Engineer:
 *    Brant Geddes
 *    200350415
 *	Date Written:
 *		2018-10-11
 *  Contains defines and functions used to setup register pointers
 *
 **/
 
#ifndef HEADER_REGISTERS_HEADER
#define HEADER_REGISTERS_HEADER

#include <stdint.h>
#include <cassert>

// Base Peripheral Bus Addresses
#define PERIPH_BASE             ((uint32_t)	        0x40000000)
#define IOPERIPH_BASE           (PERIPH_BASE +      0x10000000)
#define AHBPERIPH_BASE          (PERIPH_BASE +      0x00020000)
#define APB2PERIPH_BASE         (PERIPH_BASE +      0x00010000)
#define APB1PERIPH_BASE         (PERIPH_BASE +      0x00000000)
// Perepheral Addresses
#define RCC_BASE                (AHBPERIPH_BASE +   0x00001000) // 0x40021000
#define GPIOA_BASE              (IOPERIPH_BASE +    0x00000000) // 0x50000000
#define GPIOB_BASE              (IOPERIPH_BASE +    0x00000400) // 0x50000400
#define GPIOC_BASE              (IOPERIPH_BASE +    0x00000800) // 0x50000800
#define GPIOD_BASE              (IOPERIPH_BASE +    0x00000C00) // 0x50000C00
#define GPIOE_BASE              (IOPERIPH_BASE +    0x00001000) // 0x50001000
#define GPIOH_BASE              (IOPERIPH_BASE +    0x00001C00) // 0x50001C00
#define USART1_BASE             (APB2PERIPH_BASE +  0x00003800) // 0x40013800
#define USART2_BASE             (APB1PERIPH_BASE +  0x00004400) // 0x40004400
#define LPUART_BASE             (APB1PERIPH_BASE +  0x00004800) // 0x40004800
#define TIM2_BASE               (APB1PERIPH_BASE +  0x00000000) // 0x40000000
#define TIM3_BASE               (APB1PERIPH_BASE +  0x00000400) // 0x40000400
#define TIM21_BASE              (APB2PERIPH_BASE +  0x00000800) // 0x40010800
#define LPTIM_BASE              (APB1PERIPH_BASE +  0x00007C00) // 0x40007C00
#define ADC1_BASE               (APB2PERIPH_BASE +  0x00002400) // 0x40012400
#define PWR_BASE                (APB1PERIPH_BASE +  0x00007000) // 0x40007000
#define DMA_BASE                (AHBPERIPH_BASE	+   0x00000000) // 0x40020000
#define NVM_BASE                (AHBPERIPH_BASE +   0x00002000) // 0x40022000
#define SYSCFG_BASE             (APB2PERIPH_BASE +  0x00000000) // 0x40010000
#define EXTI_BASE               (APB2PERIPH_BASE +  0x00000400) // 0x40010400
// Other Addresses
#define NVIC_BASE               ((uint32_t)         0xE000E100) // 0xE000E100
#define SCB_BASE                ((uint32_t)         0xE000ED00) // 0xE000ED00
#define EEPROM_BASE             ((uint32_t)         0x08080000) // 0x08080000

// Peripheral register structures
typedef struct {
	volatile uint32_t CR;
	volatile uint32_t ICSCR;
	volatile uint32_t CRRCR;
	volatile uint32_t CFGR;
	volatile uint32_t CIER;
	volatile uint32_t CIFR;
	volatile uint32_t CICR;
	volatile uint32_t IOPRSTR;
	volatile uint32_t AHBRSTR;
	volatile uint32_t APB2RSTR;
	volatile uint32_t APB1RSTR;
	volatile uint32_t IOPENR;
	volatile uint32_t AHBENR;
	volatile uint32_t APB2ENR;
	volatile uint32_t APB1ENR;
	volatile uint32_t IOPSMEN;
	volatile uint32_t AHBSMENR;
	volatile uint32_t APB2SMENR;
	volatile uint32_t APB1SMENR;
	volatile uint32_t CCIPR;
	volatile uint32_t CSR;
} RCC_type;

typedef struct {
	volatile uint32_t MODER;
	volatile uint32_t OTYPER;
	volatile uint32_t OSPEEDR;
	volatile uint32_t PUPDR;
	volatile uint32_t IDR;
	volatile uint32_t ODR;
	volatile uint32_t BSRR;
	volatile uint32_t LCKR;
	volatile uint32_t AFRL;
	volatile uint32_t AFRH;
	volatile uint32_t BRR;
} GPIO_type;

typedef struct {
	volatile uint32_t CR1;
	volatile uint32_t CR2;
	volatile uint32_t CR3;
	volatile uint32_t BRR;
	volatile uint32_t GTPR;
	volatile uint32_t RTOR;
	volatile uint32_t RQR;
	volatile uint32_t ISR;
	volatile uint32_t ICR;
	volatile uint32_t RDR;
	volatile uint32_t TDR;
} USART_type;

typedef struct {
	volatile uint32_t CR1;
	volatile uint32_t CR2;
	volatile uint32_t CR3;
	volatile uint32_t BRR;
	volatile uint32_t RESERVED_0[2];
	volatile uint32_t RQR;
	volatile uint32_t ISR;
	volatile uint32_t ICR;
	volatile uint32_t RDR;
	volatile uint32_t TDR;
} LPUART_type;

typedef struct {
    volatile uint32_t CR1;
    volatile uint32_t CR2;
    volatile uint32_t SMCR;
    volatile uint32_t DIER;
    volatile uint32_t SR;
    volatile uint32_t EGR;
    volatile uint32_t CCMR1;
    volatile uint32_t CCMR2;
    volatile uint32_t CCER;
    volatile uint32_t CNT;
    volatile uint32_t PSC;
    volatile uint32_t ARR;
    volatile uint32_t RESERVED_1;
    volatile uint32_t CCR1;
    volatile uint32_t CCR2;
    volatile uint32_t CCR3;
    volatile uint32_t CCR4;
    volatile uint32_t RESERVED_2;
    volatile uint32_t DCR;
    volatile uint32_t DMAR;
    volatile uint32_t OR;
} TIMx_type;

typedef struct {
	volatile uint32_t CR1;
	volatile uint32_t CR2;
	volatile uint32_t SMCR;
	volatile uint32_t DIER;
	volatile uint32_t SR;
	volatile uint32_t EGR;
	volatile uint32_t CCMR1;
	volatile uint32_t RESERVED_1;
	volatile uint32_t CCER;
	volatile uint32_t CNT;
	volatile uint32_t PSC;
	volatile uint32_t ARR;
	volatile uint32_t RESERVED_2;
	volatile uint32_t CCR1;
	volatile uint32_t CCR2;
	volatile uint32_t RESERVED_3[5];
	volatile uint32_t OR;
} TIM2x_type;

typedef struct {
	volatile uint32_t ISR;
	volatile uint32_t ICR;
	volatile uint32_t IER;
	volatile uint32_t CFGR;
	volatile uint32_t CR;
	volatile uint32_t CMP;
	volatile uint32_t ARR;
	volatile uint32_t CNT;
} LPTIM_type;

typedef struct {
	volatile uint32_t ISR;
	volatile uint32_t IER;
	volatile uint32_t CR;
	volatile uint32_t CFGR1;
	volatile uint32_t CFGR2;
	volatile uint32_t SMPR;
	volatile uint32_t RESERVED_1[2];
    volatile uint32_t TR;
    volatile uint32_t RESERVED_2;
	volatile uint32_t CHSELR;
    volatile uint32_t RESERVED_3[5];
	volatile uint32_t DR;
    volatile uint32_t RESERVED_4[28];
	volatile uint32_t CALFACT;
    volatile uint32_t RESERVED_5[148];
	volatile uint32_t CCR;
} ADC_type;

typedef struct {
	volatile uint32_t CR;
	volatile uint32_t CSR;
} PWR_type;

typedef struct {
	volatile uint32_t ISR;
	volatile uint32_t IFCR;
	volatile uint32_t CCR1;
	volatile uint32_t CNDTR1;
	volatile uint32_t CPAR1;
	volatile uint32_t CMAR1;
			 uint32_t RESERVED_1;
	volatile uint32_t CCR2;
	volatile uint32_t CNDTR2;
	volatile uint32_t CPAR2;
	volatile uint32_t CMAR2;
			 uint32_t RESERVED_2;
	volatile uint32_t CCR3;
	volatile uint32_t CNDTR3;
	volatile uint32_t CPAR3;
	volatile uint32_t CMAR3;
			 uint32_t RESERVED_3;
	volatile uint32_t CCR4;
	volatile uint32_t CNDTR4;
	volatile uint32_t CPAR4;
	volatile uint32_t CMAR4;
			 uint32_t RESERVED_4;
	volatile uint32_t CCR5;
	volatile uint32_t CNDTR5;
	volatile uint32_t CPAR5;
	volatile uint32_t CMAR5;
			 uint32_t RESERVED_5;
	volatile uint32_t CCR6;
	volatile uint32_t CNDTR6;
	volatile uint32_t CPAR6;
	volatile uint32_t CMAR6;
			 uint32_t RESERVED_6;
	volatile uint32_t CCR7;
	volatile uint32_t CNDTR7;
	volatile uint32_t CPAR7;
	volatile uint32_t CMAR7;
			 uint32_t RESERVED_7;
	volatile uint32_t CSELR;
} DMA_type;

typedef struct {
	volatile uint32_t ACR;
	volatile uint32_t PECR;
	volatile uint32_t PDKEYR;
	volatile uint32_t PEKEYR;
	volatile uint32_t PRGKEYR;
	volatile uint32_t OPTKEYR;
	volatile uint32_t SR;
	volatile uint32_t OPTR;
	volatile uint32_t WRPROT1;
	volatile uint32_t WRPROT2;
} NVM_type;

typedef struct {
    volatile uint32_t CFGR1;
    volatile uint32_t CFGR2;
    volatile uint32_t EXTICR1;
    volatile uint32_t EXTICR2;
    volatile uint32_t EXTICR3;
    volatile uint32_t EXTICR4;
} SYSCFG_type;

typedef struct {
    volatile uint32_t IMR;
    volatile uint32_t EMR;
    volatile uint32_t RTSR;
    volatile uint32_t FTSR;
    volatile uint32_t SWIER;
    volatile uint32_t PR;
} EXTI_type;

typedef struct {
	volatile uint32_t CPUID;
	volatile uint32_t ICSR;
	volatile uint32_t VTOR;
	volatile uint32_t AIRCR;
	volatile uint32_t SCR;
	volatile uint32_t CCR;
			 uint32_t RESERVED_1;
	volatile uint32_t SHPR2;
	volatile uint32_t SHPR3;
} SCB_type;

typedef struct {
	volatile uint32_t ISER;
			 uint32_t RESERVED_1[19];
	volatile uint32_t ICER;
			 uint32_t RESERVED_2[19];
	volatile uint32_t ISPR;
			 uint32_t RESERVED_3[19];
	volatile uint32_t ICPR;
			 uint32_t RESERVED_4[19];
	volatile uint32_t IPR[8];
			 uint32_t RESERVED_5[52];
} NVIC_type;

typedef struct {
    volatile uint16_t voltage[250];
    volatile uint16_t temperature[250];
    volatile uint16_t flow[250];
    volatile uint16_t pressure[250];
    volatile uint8_t size;
} EEPROM_type;

// Peripheral structure defines
#define RCC 		((RCC_type *) RCC_BASE)
#define GPIOA		((GPIO_type *) GPIOA_BASE)
#define GPIOB		((GPIO_type *) GPIOB_BASE)
#define GPIOC		((GPIO_type *) GPIOC_BASE)
#define GPIOD		((GPIO_type *) GPIOD_BASE)
#define GPIOE		((GPIO_type *) GPIOE_BASE)
#define GPIOH		((GPIO_type *) GPIOH_BASE)
#define USART1	    ((USART_type *) USART1_BASE)
#define LPUART	    ((LPUART_type *) LPUART_BASE)
#define TIM2        ((TIMx_type *) TIM2_BASE)
#define TIM21		((TIM2x_type *) TIM21_BASE)
#define LPTIM		((LPTIM_type *) LPTIM_BASE)
#define ADC1		((ADC_type *) ADC1_BASE)
#define PWR			((PWR_type *) PWR_BASE)
#define DMA			((DMA_type *) DMA_BASE)
#define NVM			((NVM_type *) NVM_BASE)
#define EXTI        ((EXTI_type *) EXTI_BASE)
#define SYSCFG      ((SYSCFG_type *) SYSCFG_BASE)
#define SCB			((SCB_type *) SCB_BASE)
#define NVIC		((NVIC_type *) NVIC_BASE)
#define EEPROM      ((EEPROM_type *) EEPROM_BASE)


// Static assertions for struct size
static_assert((sizeof(RCC_type) == 84), "Invalid RCC struct size");
static_assert((sizeof(GPIO_type) == 44), "Invalid GPIO struct size");
static_assert((sizeof(USART_type) == 44), "Invalid USART struct size");
static_assert((sizeof(LPUART_type) == 44), "Invalid LPUART struct size");
static_assert((sizeof(TIMx_type) == 84), "Invalid TIMx struct size");
static_assert((sizeof(TIM2x_type) == 84), "Invalid TIM2x struct size");
static_assert((sizeof(LPTIM_type) == 32), "Invalid LPTIM struct size");
static_assert((sizeof(ADC_type) == 780), "Invalid ADC struct size");
static_assert((sizeof(PWR_type) == 8), "Invalid PWR struct size");
static_assert((sizeof(DMA_type) == 152), "Invalid DMA struct size");
static_assert((sizeof(NVM_type) == 40), "Invalid NVM struct size");
static_assert((sizeof(SYSCFG_type) == 24), "Invalid SYSCFG struct size");
static_assert((sizeof(EXTI_type) == 24), "Invalid EXTI struct size");
static_assert((sizeof(SCB_type) == 36), "Invalid SCB struct size");
//static_assert((sizeof(NVIC_type) == 960), "Invalid NVIC struct size");
static_assert((sizeof(EEPROM_type) == 2002), "Invalid EEPROM struct size");


#endif     

//EOF

