
#include "registers.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_PWRDRIVER_HEADER
#define HEADER_PWRDRIVER_HEADER

namespace driver {
    namespace PWRDriver {
        class PWRDriver : public Driver {
            
            private:
            RCCDriver::RCCDriver rcc;
            
            public:
            PWRDriver();
            ~PWRDriver();
            
            virtual void open();
            virtual void close();
            virtual void start();
            virtual void stop();
            void sleep();
            
        };
    }
}

#endif 

// EOF
