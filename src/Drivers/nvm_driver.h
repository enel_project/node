
#include "registers.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_NVMDRIVER_HEADER
#define HEADER_NVMDRIVER_HEADER

namespace driver {
    namespace NVMDriver {
        class NVMDriver : public Driver {
            public:
            NVMDriver();
            ~NVMDriver();
            
            virtual void open();
            virtual void close();
            virtual void start();
            virtual void stop();
            
        };
    }
}

#endif 

// EOF
