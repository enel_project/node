
#include "registers.h"
#include "rcc_driver.h"
#include "pwr_driver.h"
#include <stdint.h>

namespace driver {
    namespace PWRDriver {
        PWRDriver::PWRDriver() : Driver() {}
        PWRDriver::~PWRDriver() {}
                
        void PWRDriver::open() {
            SCB->SCR |= (1u << 2);
        }
        void PWRDriver::close() {}
        void PWRDriver::start() { 
          //  PWR->CR &= ~(3u << 11);
          //  PWR->CR |= (1u << 11);
        }
        void PWRDriver::stop() {
           // PWR->CR &= ~(3u << 11);
          //  PWR->CR |= (3u << 11);
        }
        void PWRDriver::sleep() { 
            rcc.open(RCCDriver::MSI);
            rcc.sysclk(RCCDriver::MSI);
            rcc.close(RCCDriver::HSI16);
            rcc.close(RCCDriver::LSI);
            PWR->CR &= ~(3u << 11);
            PWR->CR |= (3u << 11); 
            __asm("WFI"); 
            PWR->CR &= ~(3u << 11);
            PWR->CR |= (1u << 11);
            rcc.open(RCCDriver::HSI16);
            rcc.open(RCCDriver::LSI);
            rcc.sysclk(RCCDriver::HSI16);
        }
    }
}

// EOF
