
#include "registers.h"
#include "rcc_driver.h"
#include "gpio_driver.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_TIMDRIVER_HEADER
#define HEADER_TIMDRIVER_HEADER

namespace driver {
	namespace TIMDriver {
		class TIMDriver : public Driver {
			
			public:
			TIMDriver();
			~TIMDriver();
			
			virtual void open();
			virtual void close();
			virtual void start();
			virtual void stop();
			uint16_t read();
			private:
				
			RCCDriver::RCCDriver rcc;
			driver::GPIODriver::GPIODriver gpioa;
			
		};
	}
}

#endif

// EOF
