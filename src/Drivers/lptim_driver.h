
#include "registers.h"
#include "rcc_driver.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_LPTIMDRIVER_HEADER
#define HEADER_LPTIMDRIVER_HEADER

namespace driver {
    namespace LPTIMDriver {
        class LPTIMDriver : public Driver {
            
            public:
            LPTIMDriver();
            ~LPTIMDriver();
            
            virtual void open();
            virtual void close();
            virtual void start();
            void start(uint16_t cmp);
            virtual void stop();
            uint16_t getTimeE();
            uint16_t getTimeS();
            bool returnFlag();
            private:
            RCCDriver::RCCDriver rcc;
            
        };
    }
}

#endif

// EOF
