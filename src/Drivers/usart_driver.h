
#include "registers.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_USARTDRIVER_HEADER
#define HEADER_USARTDRIVER_HEADER

namespace driver {
	class USARTDriver : public Driver {
		
		USARTDriver();
		~USARTDriver();
		
		virtual void open();
		virtual void close();
		virtual void start();
		virtual void stop();
		
	};
}

#endif 

// EOF
