
#include "registers.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_RCCDRIVER_HEADER
#define HEADER_RCCDRIVER_HEADER

namespace driver {
	namespace RCCDriver {
		
		typedef enum {
			MSI, HSI16, HSE, PLL, 
			HSI48, LSI, LSE
		} clock_t;
		
		typedef enum {
			IOPA, IOPB, IOPC, IOPD, IOPE, IOPH, // GPIO
			NVMP, DMAP,                         // AHB
			USARTP, ADCP, TIM21P, SYSCFGEN,     // APB2
			LPTIMP, PWRP, LPUARTP, TIM2P        // APB1
		} perepheral_t;

		typedef enum {
			PLLx3, PLLx4, PLLx6, PLLx8, PLLx12, 
			PLLx16, PLLx24, PLLx32, PLLx48, 
		} pllMul_t;
		
		typedef enum {
			PLL_NOT_ALLOWED1, PLLd2, PLLd3, PLLd4
		} pllDiv_t;
		
		class RCCDriver : public Driver {
			
			public:
			
			RCCDriver();
			~RCCDriver();
			
			void open(clock_t clock);
			void close();
			void close(clock_t clock);
			void sysclk(clock_t clock);
			void pll(clock_t clock, pllMul_t mul, pllDiv_t div);
			void start(perepheral_t pereph);
			void stop();
			void stop(perepheral_t pereph);
			
		};
	}
}

#endif

// EOF
