
#include "registers.h"
#include "rcc_driver.h"
#include "lptim_driver.h"
#include <stdint.h>
bool wakeUpTime = false;
using driver::RCCDriver::LPTIMP;

namespace driver {
    namespace LPTIMDriver {
        LPTIMDriver::LPTIMDriver() : Driver(), rcc() {}
        LPTIMDriver::~LPTIMDriver() {}
            
        bool LPTIMDriver::returnFlag(){
                    return wakeUpTime;
            }
        void LPTIMDriver::open() {
            // *1* Start lptim clock
            // *2* Set clock prescaler
            // *3* Enable arr match interrupt
            // *4* Unmask timer interrupt
            // *5* Enable lptim
            
            rcc.start(LPTIMP);          // *1*
            
            LPTIM->CFGR &= ~(7u << 9);  // *2*
            LPTIM->CFGR |= (7u << 9);   // *2*
            LPTIM->CFGR |= (1u << 19);
            LPTIM->IER |= (1u << 1);    // *3*
            NVIC->ISER |= (1u << 13);   // *4*
            EXTI->IMR |= (1u << 29);    // *4*
            LPTIM->CR |= (1u << 0);     // *5*
        
        }
        void LPTIMDriver::close() {
            // *1* Set all lptim values to default
            // *2* Mask LPTIM interrupt
            // *3* Disable LPTIM
            // *4* Stop lptim clock
            LPTIM->CFGR = 0x00000000;   // *1*
            LPTIM->IER =  0x00000000;   // *1*
            LPTIM->CMP =  0x00000000;   // *1*
            LPTIM->ARR =  0x00000001;   // *1*
            NVIC->ICER =  (1u << 13);   // *2*
            EXTI->IMR &= ~(1u << 29);   // *2*
            LPTIM->CR &= ~(1u << 0);    // *3*
            rcc.stop(LPTIMP);           // *4*
        }
        void LPTIMDriver::start() { assert(false); }
        void LPTIMDriver::start(uint16_t cmp) {
            // *1* Set compare value
            // *2* Start timer in One-Shot mode
            LPTIM->CR |= (1u << 0);
            LPTIM->ARR = cmp;           // *1*
            wakeUpTime = false;
            LPTIM->CR |= (1u << 1);     // *2*   
        }
        void LPTIMDriver::stop() {
            LPTIM->CR &= ~(1u << 0);
        }
        uint16_t LPTIMDriver:: getTimeE(){
            // *1* Return current timer value
            return LPTIM->CNT;          // *1*
        }
        uint16_t LPTIMDriver:: getTimeS(){
            // *1* Return current timer value
            return LPTIM->ARR;          // *1*
        }
    }
}

extern "C" {
    void LPTIM1_IRQHandler(void) {
        // *1* Clear interrupt pending
        LPTIM->ICR |= (1u << 1);        // *1*
        wakeUpTime = true;
        
    }
}

// EOF
