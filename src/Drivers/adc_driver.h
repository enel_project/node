
#ifndef HEADER_ADCDRIVER_HEADER
#define HEADER_ADCDRIVER_HEADER

#include "registers.h"
#include "rcc_driver.h"
#include "gpio_driver.h"
#include "dma_driver.h"
#include "driver.h"
#include <stdint.h>

namespace driver {
    namespace ADCDriver {
        class ADCDriver : public Driver {
            
            public:
                
            ADCDriver();
            ~ADCDriver();
            
            virtual void open();
            virtual void close();
            virtual void start();
            virtual void stop();
            void calibrate();
            bool read_ready();
            uint16_t read(uint8_t channel);
            
            private:
                
            RCCDriver::RCCDriver rcc;
            GPIODriver::GPIODriver gpioa;
        };
    }
}
#endif

// EOF
