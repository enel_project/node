
#include "registers.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_DMADRIVER_HEADER
#define HEADER_DMADRIVER_HEADER

namespace driver {
    namespace DMADriver {
        class DMADriver : public Driver {
            
            public:
                
            DMADriver();
            ~DMADriver();
            
            virtual void open();
            virtual void close();
            virtual void start();
            virtual void stop();
            
        };
    }
}

#endif 

// EOF
