
#include "registers.h"
#include "driver.h"
#include <stdint.h>

#ifndef HEADER_NVICDRIVER_HEADER
#define HEADER_NVICDRIVER_HEADER

namespace driver {
    namespace NVICDriver {
        class NVICDriver : public Driver {
            
            NVICDriver();
            ~NVICDriver();
            
            virtual void open();
            virtual void close();
            virtual void start();
            virtual void stop();
            
        };
    }
}

#endif

// EOF
