
#include "registers.h"
#include "gpio_driver.h"
#include "rcc_driver.h"
#include "lpuart_driver.h"
#include <stdint.h>

static const uint32_t LPUART_TC = (1u << 6);
static const uint32_t LPUART_TXE = (1u << 7);

static uint8_t idx = 0;
static uint8_t length = 0;
static unsigned char ring[100];

namespace driver {
	namespace LPUARTDriver {
		LPUARTDriver::LPUARTDriver() : Driver(), rcc(), gpiob(GPIODriver::PORTB) {
			idx = 0;
			length = 0;
		}
		LPUARTDriver::~LPUARTDriver() {} //this->close();  }
				
		void LPUARTDriver::open() {
			// PB10 -> TX
			// PB11 -> RX
		
			rcc.start(RCCDriver::IOPB);
			rcc.start(RCCDriver::LPUARTP);
			
			gpiob.open(10, GPIODriver::ALTERNATE, GPIODriver::PUSHPULL, GPIODriver::VHIGH, GPIODriver::NONE, GPIODriver::AFIO4);
			gpiob.open(11, GPIODriver::ALTERNATE, GPIODriver::PUSHPULL, GPIODriver::VHIGH, GPIODriver::NONE, GPIODriver::AFIO4);
			
			LPUART->BRR = 0x682AA;	// set BAUD rate (9600);
			LPUART->CR1 |= (1u << 5); // Enable RXNE interrupt
			LPUART->CR3 |= (1u << 12); // Disable over run detections
			
		}
		void LPUARTDriver::close() {
            // *1* Mask LPUART interrupt
            // *2* Set all LPUART registers to default
            // *3* Set all LPUART IO to default
            // *4* Turn off LPUART clock
            
            NVIC->ICER |= (1u << 29);       // *1*
            LPUART->CR1 = 0x00000000;       // *2*
            LPUART->CR2 = 0x00000000;       // *2*
            LPUART->CR3 = 0x00000000;       // *2*
            gpiob.close(10);                // *3*
            gpiob.close(11);                // *3*
            rcc.stop(RCCDriver::LPUARTP);   // *4*
            
        }
		void LPUARTDriver::start() {
			
			rcc.start(RCCDriver::IOPB);
			rcc.start(RCCDriver::LPUARTP);
			
			NVIC->ISER |= (1u << 29); // Unmask LPUART interrupt
			LPUART->CR1 |= (1u << 3); // Turn transmitter on
			LPUART->CR1 |= (1u << 2); // Turn reciever on
			LPUART->CR1 |= (1u << 0); // Enable LPUART
			
		}
		void LPUARTDriver::stop() {
			
			//while (~(LPUART->ISR & LPUART_TC));
			LPUART->CR1 &= ~(1u << 3); // Turn transmitter off
			LPUART->CR1 &= ~(1u << 2); // Turn reciever off
			LPUART->CR1 &= ~(1u << 0); // Disable LPUART
            NVIC->ICER |= (1u << 29); // Mask LPUART interrupt
			rcc.stop(RCCDriver::LPUARTP);
			
		}
		unsigned char LPUARTDriver::read() {
			if (length > 0) {
				idx = (idx == 0) ? 99 : idx - 1;
				unsigned char character = ring[idx];
				length--;
				return character;
			} else {
				return '\xFF';
			}
		}
		void LPUARTDriver::write(unsigned char message) {
			LPUART->TDR = message;
		}
		bool LPUARTDriver::read_ready() {
			return (length > 0);
		}
		bool LPUARTDriver::write_ready() {
			return !!(LPUART->ISR & LPUART_TXE);
		}
		uint8_t LPUARTDriver::get_length() {
			return length;
		}
		void LPUARTDriver::clear_buffer() {
			length = 0;
		}
	}
}


extern "C" {
	void RNG_LPUART1_IRQHandler (void) {
		ring[idx++] = LPUART->RDR;
		idx %= 100;
		length = (length > 99) ? 100 : length + 1;
		NVIC->ICPR |= (1u << 29);
	}
}

// EOF
