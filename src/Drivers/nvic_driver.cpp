
#include "registers.h"
#include "nvic_driver.h"
#include <stdint.h>

namespace driver {
    namespace NVICDriver {
        NVICDriver::NVICDriver() : Driver() {}
        NVICDriver::~NVICDriver() {}
                
        void NVICDriver::open() {}
        void NVICDriver::close() {}
        void NVICDriver::start() {}
        void NVICDriver::stop() {}
    }
}

// EOF
