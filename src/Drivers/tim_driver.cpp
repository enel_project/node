
#include "registers.h"
#include "tim_driver.h"
#include "rcc_driver.h"
#include "gpio_driver.h"
#include <stdint.h>

// Set timer to be used
volatile static TIMx_type *TIM = TIM2;

// Include necessary constants from different namespaces
using driver::GPIODriver::PORTA;
using driver::GPIODriver::ALTERNATE;
using driver::GPIODriver::PUSHPULL;
using driver::GPIODriver::HIGH;
using driver::GPIODriver::NONE;
using driver::GPIODriver::AFIO2;
using driver::RCCDriver::TIM2P;
using driver::RCCDriver::IOPA;

namespace driver {
	namespace TIMDriver {
		TIMDriver::TIMDriver() : Driver(), rcc(), gpioa(PORTA) {}
		TIMDriver::~TIMDriver() {}
				
		void TIMDriver::open() {
			// *1* Enable timer 2 clock
			// *2* Enable GPIOA clock
			// *3* Configure PA3 as alternate input (Timer 2 - CH4)
			// *4* Configure channel 4 in input mode, mapped to TI4
			// *5* Configure TI4 to trigger on rising edge polarity
			// *6* Enable counter capture
            // *7* Configure timer in external clock input mode 1
			// *8* Configure TI2 as trigger used to synchronize counter
            // *9* Set auto-reload register to maximum value
			
			rcc.start(TIM2P); 			// *1*
			rcc.start(IOPA);		    // *2*
                                        // *3*
			gpioa.open(3, ALTERNATE, PUSHPULL, HIGH, NONE, AFIO2);
			TIM->CCMR2 &= ~(3u << 8);   // *4*
			TIM->CCMR2 |= (1u << 8);	// *4*
			TIM->CCER &= ~(1u << 13);	// *5*
			TIM->CCER &= ~(1u << 15);	// *5*
            TIM->CCER |= (1u << 12);     // *6*
			TIM->SMCR |= (7u << 0); 	// *7*
			TIM->SMCR &= ~(7u << 4); 	// *8*
			TIM->SMCR |= (6u << 4);		// *8*
            TIM->ARR = 0xFFFF;          // *9*
			
		}
		void TIMDriver::close() {
			// *1* Reset timer registers to default values
			// *2* Turn off timer clock
		
			TIM->CR1 = 		0x00000000;	// *1*
			TIM->CCMR1 = 	0x00000000; // *1*
			TIM->CCER =  	0x00000000; // *1*
			TIM->SMCR = 	0x00000000; // *1*
			rcc.stop(TIM2P); 			// *2*
		}
		void TIMDriver::start() {
			// *1* Start timer clock
			// *2* Start GPIOA clock
			// *3* Enable timer
			
			rcc.start(TIM2P); 	        // *1*
			rcc.start(IOPA);			// *2*
			TIM->CR1 |= (1u << 0);      // *3*
			
		}
		void TIMDriver::stop() {
			// *1* Stop timer
			// *2* Stop timer clock
      
			TIM->CR1 &= ~(1u << 0);		// *1*
			rcc.stop(TIM2P);			// *2*
			
		}
        uint16_t TIMDriver::read() {    
            return TIM->CNT;
        }
	}
}

// EOF
