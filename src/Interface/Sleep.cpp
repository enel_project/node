/*  Author Quinn Bancescu 
    Description: This File controls the timer and sleep mode
*/


#include "Sleep.h"


Sleep::Sleep():pwr(), timer(){
    pwr.open();
    timer.open();
}

Sleep::~Sleep(){
    timer.close();
    pwr.close();
    
}

void Sleep::set(int time){   
    timer.stop();
    gtime = time * 278;
}

void Sleep::sleep(){
    pwr.sleep();
}
void Sleep::startTimer(){
    timer.start(gtime);
}
uint16_t Sleep::getTimeElasped(){
    return timer.getTimeE();
}
uint16_t Sleep::getTimeSet(){
    return timer.getTimeS();
}


bool Sleep::ReturnFlag(){
    return timer.returnFlag();
}

