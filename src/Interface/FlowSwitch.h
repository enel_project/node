/*  Author Quinn Bancescu 
    Description: This File is responsible for controling the flow switch
*/

#include "../Drivers/perepherals.h"

#ifndef HEADER_FLOWSWITCH_HEADER
#define HEADER_FLOWSWITCH_HEADER

class FlowSwitch {
    private:
        driver::GPIODriver::GPIODriver gpioB;
        bool FlowWake;
    public:
        FlowSwitch();
        ~FlowSwitch();
        bool readFlowSwitch();
        bool getFlow();
        void MaskHandler();
        void unMaskHandler();
};

#endif


