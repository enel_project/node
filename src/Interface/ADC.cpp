/*  Author Quinn Bancescu 
    Description: This File is responsible for controling the ADC
*/

#include "ADC.h"
using driver::GPIODriver::PORTB;
using driver::GPIODriver::PULLUP;
using driver::GPIODriver::OPENDRAIN;
using driver::GPIODriver::LOW;
using driver::GPIODriver::OUTPUT;

ADC::ADC(): adc(),gpioB(PORTB){
    gpioB.open(4,OUTPUT, OPENDRAIN, LOW, PULLUP);
    adc.open();
    Tempature = 0;
    Pressure = 0;
    Voltage = 0;
    gpioB.set(4);
    }
    ADC::~ADC(){
    }
    void ADC::readVolatage(){
        Voltage = adc.read(0);
    }
    void ADC::readPressure(){
        Pressure = adc.read(2);
    }
    void ADC::readTempature(){
        Tempature = adc.read(1);
    }
    uint16_t ADC::getVoltage(){
        return Voltage;
        
    }
    uint16_t ADC::getPressure(){
        return Pressure;
        
    }
    uint16_t ADC::getTempature(){
        return Tempature;
        
    }
    void ADC::read(){
        while(!adc.read_ready()){
        }
        readPressure();
        readTempature();
        readVolatage();
        adc.stop();
        //gpioB.rset(4);
    }
    void::ADC::adcStart(){
       // gpioB.set(4);
       // for(volatile int i = 0; i < 1000000; i++);
        adc.start();  
    }
