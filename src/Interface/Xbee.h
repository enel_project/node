/*  Author Quinn Bancescu 
    Description: This File is responsible for controling the xbee
*/


#include "../Drivers/perepherals.h"
#include <cstring>

#ifndef HEADER_XBEE_HEADER
#define HEADER_XBEE_HEADER



struct Message {
	public:
    unsigned char message[100];
    uint16_t size;
		Message(char data[], int length){ std::memcpy(message,data,length);  size = length;};
		Message(){size = 0;};
};


class XBeeInterface {
    public:
    XBeeInterface();
    ~XBeeInterface();
    void XBeeSleep();
    void XBeeWake();
    void XBeeSend(unsigned char character);
    void XBeeSend(Message &data);
    void XBeeRecieve(Message &data);
    unsigned char MacAdress[17];
    uint8_t getUArtLength();
    void resetBuffer();
    void waitForCoordinator();
    private:
    driver::LPUARTDriver::LPUARTDriver uart;
    driver::GPIODriver::GPIODriver gpioB;
};

#endif
