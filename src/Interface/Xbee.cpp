/*  Author Quinn Bancescu 
    Description: This File is responsible for controling the xbee
*/


#include "Xbee.h"
#include <stdio.h>
#include <cstring>
using driver::GPIODriver::PORTB;
using driver::GPIODriver::PULLUP;
using driver::GPIODriver::OPENDRAIN;
using driver::GPIODriver::LOW;
using driver::GPIODriver::OUTPUT;

XBeeInterface::XBeeInterface() 
    : gpioB(PORTB) {
    // *1* create var for to send commands
    // *2* create var for xbee responces
    // *3* wait for bootloader to time out
    // *4* enable uart
    // *5* configure PB3 as a opendrain output
    // *6* start the uart
    // *7* send command to enter command mode of xbee
    // *8* wait for expected responce 
    // *9* store responce into responce var
    // *10* overide command var to new command
    // *11* send command to xbee for higher part of mac
    // *12* wait for known responce
    // *13* recieve responce
    // *14* add responce to mac address
    // *15* overide command var to new command
    // *16* send command 
    // *17* wait for known responce
    // *18* recieve responce
    // *19* copy responce to mac adress
    // *20* overide command var to new command
    // *21* send command 
    // *22* wait for known responce
    // *23* recieve responce
    Message commands("+++", 3);                         // *1*
    Message responce;                                   // *2*
    for(volatile int i =0; i < 5500000;i++){            // *3*
		}
	uart.open();                                    // *4*
    gpioB.open(3,OUTPUT, OPENDRAIN, LOW, PULLUP);       // *5*
    uart.start();                                       // *6*
    XBeeSend(commands);                                 // *7*
		while (uart.get_length() != 3){                  // *8*
		}
    XBeeRecieve(responce);                              // *9*
   // uart.clear_buffer();
    std::memcpy(commands.message,"ATSH\r",5);           // *10*
		commands.size = 5;
    XBeeSend(commands);                                 // *11*
		while (uart.get_length() != 7){                  // *12*
		}
    XBeeRecieve(responce);                              // *13*
  //  uart.clear_buffer();                                            // *14*
    std::memcpy(MacAdress, responce.message, (responce.size-1));
	std::memcpy(commands.message,"ATSL\r",5);       // *15*
    XBeeSend(commands);                                 // *16*
		while (uart.get_length() != 9){                  // *17*
		}
     XBeeRecieve(responce);                             // *18*
  //   uart.clear_buffer();                                                   // *19*
    std::memcpy(MacAdress+6, responce.message, (responce.size-1));
//	std::memcpy(commands.message,"ATMY\r",5);
//     XBeeSend(commands);   
//    while (uart.get_length() != 3){                  // *17*
//		}
//    XBeeRecieve(responce);  
  //  uart.clear_buffer();    
//    while(std::memcmp(responce.message,"FFF",3) == 0){
//    for(volatile int i =0; i < 10000; i++){}
//    XBeeSend(commands);   
//    while (uart.get_length() != 3){                  // *17*
//		}
//    XBeeRecieve(responce); 
 //   uart.clear_buffer();
  //  }
    std::memcpy(commands.message,"ATCN\r",5);       // *20*
    XBeeSend(commands);                                 // *21*
		while (uart.get_length() !=3){                  // *22*
		}
        XBeeRecieve(responce); 
   //  waitForCoordinator();
   //     uart.clear_buffer();        // *23* 
}

XBeeInterface::~XBeeInterface() {
    uart.close();
}

void XBeeInterface::XBeeSleep() {
    // *1* set the PB3 pin to high puts xbee to sleep
    gpioB.set(3);   // *1*
    uart.stop();
}

void XBeeInterface::XBeeWake() {
    // *1* sets the PB3 pin to low wakes xbee up
    
    gpioB.rset(3);
    uart.start();
}

void XBeeInterface::XBeeSend(unsigned char character) {
    while(!uart.write_ready());
    uart.write(character);
}

void XBeeInterface::XBeeSend(Message &data) { 
    // *1* for loops runs as many times as the data size
    // *2* waits to the uart to be ready
    // *3* write character to the uart
    for(int i = 0; i < data.size ; i++) {       // *1*
        while(uart.write_ready() != 1){         // *2*
        }
        uart.write(data.message[i]);         // *3*
        for( volatile int i =0; i < 3000; i++){
        }
    }
}

void XBeeInterface::XBeeRecieve(Message &data) {
    // *1* sets inital size of data to 0
    // *2* inititalzes the flag var to the state of uart flag
    // *3* waits for uart flag to be 1 aka ready
    // *4* set length var to uart buffer length
    // *5* move characters from buffer to data 
    // *6* increase data size
    data.size=0;                                    // *1*
    int flag = uart.read_ready();                   // *2*
    while(flag ==0) {	                            // *3*
			 flag = uart.read_ready();
		}
		uint8_t length = (uart.get_length()-1);     // *4*
		for(volatile int i = length; i >= 0; i--){  // *5*
			data.message[i] = uart.read();
      data.size++;                                  // *6*
    }
}

uint8_t XBeeInterface::getUArtLength(){
    
    return uart.get_length();
    }
void XBeeInterface::resetBuffer(){
    uart.clear_buffer();
}

void XBeeInterface::waitForCoordinator(){
    Message commands("+++", 3);                         // *1*
    Message responce;
    XBeeSend(commands);
    while (uart.get_length() != 3){                  // *8*
		}
    XBeeRecieve(responce); 
    std::memcpy(commands.message,"ATMP\r",5);
    commands.size = 5;
    XBeeSend(commands);        
    while (uart.get_length() != 2){                  // *8*
		}
    XBeeRecieve(responce); 
        if( responce.message[0] == 'F' && responce.message[1] == 'F') {
            std::memcpy(commands.message,"ATCN\r",5);
            XBeeSend(commands);
            for( int i  = 0; i < 1000000; i++ );
            waitForCoordinator();
        } else {
            std::memcpy(commands.message,"ATCN\r",5);
            XBeeSend(commands);
            resetBuffer();
        }
}
