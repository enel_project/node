/*  Author Quinn Bancescu 
    Description: This File is responsible for controling the flow switch
*/


#include "FlowSwitch.h"

using driver::GPIODriver::PORTB;
using driver::GPIODriver::PULLDOWN;
using driver::GPIODriver::PUSHPULL;
using driver::GPIODriver::LOW;
using driver::GPIODriver::INPUT;

extern "C" { static void flow_handler(); }
static bool flowHandler; 

FlowSwitch::FlowSwitch() :gpioB(PORTB){
    // *1* Configures PB10 as pushpull input (note this is not correct as of yet, nvic drivers arnt writen yet)
   gpioB.open(0 ,INPUT, PUSHPULL, LOW, PULLDOWN);    // *1*
   gpioB.unmask(0, flow_handler);
   flowHandler = false;
}


FlowSwitch::~FlowSwitch(){
}

bool FlowSwitch:: getFlow(){
    return flowHandler;
    }
void FlowSwitch:: MaskHandler() {
    gpioB.mask(0);
    flowHandler = false;
}

bool FlowSwitch::readFlowSwitch(){
    // return bool value of input 1 is on 0 is off
    return gpioB.read(0);  // *1*
}

void FlowSwitch::unMaskHandler(){
     gpioB.unmask(0, flow_handler);
}

extern "C" {
    static void flow_handler() {
         flowHandler = true;
    }
}
