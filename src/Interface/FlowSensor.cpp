/*  Author Quinn Bancescu 
    Description: This File is responsible for controling the flow sensor
*/


#include "FlowSensor.h"


using driver::GPIODriver::PORTA;
using driver::GPIODriver::PORTB;
using driver::GPIODriver::NONE;
using driver::GPIODriver::PUSHPULL;
using driver::GPIODriver::HIGH;
using driver::GPIODriver::LOW;
using driver::GPIODriver::INPUT;
using driver::GPIODriver::OUTPUT;
using driver::GPIODriver::PULLDOWN;
extern "C" { static void flowsensor_handler(); }
static uint16_t FlowCount;
static bool pulse = false;

FlowSensor::FlowSensor() :gpioA(PORTA) ,gpioB(PORTB) {  
    gpioA.open(3,INPUT,PUSHPULL,HIGH,NONE);
    gpioB.open(2,OUTPUT,PUSHPULL,LOW,NONE);
    FlowCount = 0;
    
}

FlowSensor::~FlowSensor(){
}

uint16_t FlowSensor::getFlow(){
    // *1* returns uint that reflex the flow through the flow sensor
    return FlowCount;    //*1*
}
void FlowSensor::resetFlow(){
    // *1* resets private member var Flow to 0
    FlowCount = 0;    //*1*
    gpioA.mask(3);
    gpioB.rset(2);
}

void FlowSensor::startFlow(){
    // *1* starts the timer
    // *2* waits for the flow switch to return 0, this means there is no more flow
    // *3* reads the value from the timer
    // *4* resets the value in the timer
    // *5* stops the timer
     gpioB.set(2);
     gpioA.unmask(3,flowsensor_handler);
}

bool FlowSensor::checkFlow(){
    bool temp = pulse;
    pulse = false;
    return temp;
}
extern "C" {
static void flowsensor_handler(){
    pulse = true;
    FlowCount++;
    
}
}
