/*  Author Quinn Bancescu 
    Description: This File controls the timer and sleep mode
*/

#include "../Drivers/perepherals.h"

#ifndef HEADER_SLEEP_HEADER
#define HEADER_SLEEP_HEADER

class Sleep{
    private:
        driver::PWRDriver::PWRDriver pwr;
        driver::LPTIMDriver::LPTIMDriver timer;
        int gtime;
    public:
        Sleep();
        ~Sleep();
        void set(int time);
        void sleep();
        void startTimer();
        uint16_t getTimeElasped();
        bool ReturnFlag();
        uint16_t getTimeSet();
};

#endif
