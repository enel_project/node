/*  Author Quinn Bancescu 
    Description: This File defines the sensor reading struct
*/
#include <stdint.h>

#ifndef HEADER_dataSENSORS_HEADER
#define HEADER_dataSENSORS_HEADER

struct Data {
    public:
    uint16_t Voltage[20];
    uint16_t Pressure[20];
    uint16_t Tempature[20];
    uint16_t Flow[20];
    uint16_t Time[20];
    uint8_t size;
    
    Data(){
        for(int i = 0; i <100; i++){
           Pressure[i] = 0;
           Tempature[i]=0;
           Voltage[i] = 0;
           Flow[i] = 0;
        }    
        size = 0;
    }
    
    
};

#endif
