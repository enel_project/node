/*  Author Quinn Bancescu 
    Description: This File is responsible for controling the ADC
*/

#include "../Drivers/perepherals.h"

#ifndef HEADER_SENSOR_HEADER
#define HEADER_SENSOR_HEADER


class ADC {
    private:
        driver::ADCDriver::ADCDriver adc;
        driver::GPIODriver::GPIODriver gpioB;
        uint16_t Voltage;
        uint16_t Pressure;
        uint16_t Tempature;
        void readVolatage();
        void readPressure();
        void readTempature();
    public:
        ADC();
        ~ADC();
        void adcStart();
        void read();
        uint16_t getVoltage();
        uint16_t getPressure();
        uint16_t getTempature();
        void reset();
        
};

#endif 
