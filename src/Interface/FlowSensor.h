#include "../Drivers/perepherals.h"



#ifndef HEADER_FLOWSENSOR_HEADER
#define HEADER_FLOWSENSOR_HEADER

class FlowSensor {
    private:
        driver::GPIODriver::GPIODriver gpioA;
        driver::GPIODriver::GPIODriver gpioB;
        
    public:
        FlowSensor();
        ~FlowSensor();
        uint16_t getFlow();
        void resetFlow();
        void startFlow();
        bool checkFlow();
};
   
#endif
        

