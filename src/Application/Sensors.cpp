/*  Author Quinn Bancescu 
    Description: This File is responsible for reading and storing the sensor data
*/


#include "Sensors.h"


Sensor::Sensor(): TVPdata(), flow() {
    Sensordata.size=0;
}

 Sensor::~Sensor(){
 }
 
 void Sensor::getData( Data &stuff){
    
     // *1* pass the data structre to the packet class
     
     stuff = Sensordata;    
     for ( int i = 0; i < Sensordata.size; i++){
     Sensordata.Pressure[i] = 0;      
     Sensordata.Tempature[i] = 0;    
     Sensordata.Voltage[i] = 0;     
     Sensordata.Flow[i] = 0;                
     Sensordata.Time[i] = 0;
     Sensordata.size  = 0;
     }
 }
 
 void Sensor::startData(){
    
    // *1* start adc
    // *2* enable external interupt for slow sensor
     TVPdata.adcStart();        // *1*
     flow.startFlow();          // *2*
}
void Sensor::readData(uint16_t time){
    // *1* Tell adc to read values 
    // *2* Save value from adc
    // *3* get vaule from flow sensor
    // *4* mask external interupt from flow sensor 
    // *5* increase index of data structure
    
     TVPdata.read();                                                    // *1*
     Sensordata.Pressure[Sensordata.size] = TVPdata.getPressure();      // *2*
     Sensordata.Tempature[Sensordata.size] = TVPdata.getTempature();    // *2*
     Sensordata.Voltage[Sensordata.size] = TVPdata .getVoltage();       // *2*
     Sensordata.Flow[Sensordata.size] = flow.getFlow();                 // *3*
     Sensordata.Time[Sensordata.size] = time;
     flow.resetFlow();                                                   // *4*
     Sensordata.size++;                                                  // *5*
}

void Sensor::TestCase1(){
    
    for( int i = 0; i < 1 ; i++) {
        Sensordata.Flow[i] = 0xAAB;
        Sensordata.Pressure[i] = 0xBBC;
        Sensordata.Tempature[i] = 0xCCD;
        Sensordata.Voltage[i] = 0xDDE;
        Sensordata.Time [i] = 0xEEF;
    }
    Sensordata.size = 1;
    
}


void Sensor::TestCase3(){
    
     for( int i = 0; i < 1 ; i++) {
        Sensordata.Flow[i] = 0xFFFF;
        Sensordata.Pressure[i] = 0xFFFF;
        Sensordata.Tempature[i] = 0xFFFF;
        Sensordata.Voltage[i] = 0xFFFF;
        Sensordata.Time [i] = 0xFFFF;
    }
    Sensordata.size = 1;
    
}

void Sensor::TestCase2(){
     for( int i = 0; i < 4 ; i++) {
        Sensordata.Flow[i] = 0xAA;
        Sensordata.Pressure[i] = 0xBB;
        Sensordata.Tempature[i] = 0xCC;
        Sensordata.Voltage[i] = 0xDD;
        Sensordata.Time [i] = 0xEE;
    }
    Sensordata.size = 4;
}

bool Sensor::checkFlow(){
    return flow.checkFlow();
}
