/*  Author Quinn Bancescu 
    Description: This File is responsible for reading and storing the sensor data
*/


#include "../Interface/Interface.h"

#ifndef HEADER_SENSORS_HEADER
#define HEADER_SENSORS_HEADER

class Sensor {
    private:
    ADC TVPdata;
    FlowSensor flow;
    Data Sensordata;
    public:
    Sensor();
    ~Sensor();
    void getData(Data &stuff);
    void readData(uint16_t time);
    void startData();
    void TestCase1();
    void TestCase2();
    void TestCase3();
    bool checkFlow();
};

#endif
