/*  Author Quinn Bancescu 
    Description: This File is responsible controling the timer
*/

#include "../Interface/Interface.h"
#ifndef HEADER_LOWPOWER_HEADER
#define HEADER_LOWPOWER_HEADER

class LowPower{
    private:
        Sleep timer;
    public:
        int TimeSet;
        int FlowWakeupTime;
        int TimeRemaining;
        LowPower();
        ~LowPower();
        void Sleep();
        void Settime( );
        void startTime();
        void flowWakeUp();
        void flowShutDown();
        uint16_t getTime();
        bool CheckFlag();
};


#endif

