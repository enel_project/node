/*  Author Quinn Bancescu 
    Description: This File is responcable controling the timer
*/


#include "LowPower.h"
bool firstSet = false;
bool flowWakeUpFlag = false;

LowPower::LowPower():timer(){
    this->TimeSet = 60;
}

LowPower::~LowPower(){
}

void LowPower::Sleep(){  
   if(CheckFlag() == 0x00){
    timer.sleep();
   }
}

void LowPower::Settime(){
    if(!firstSet && !flowWakeUpFlag){
        if( TimeSet > 180){
            TimeRemaining = this->TimeSet - 180;
            timer.set(180);
            firstSet = true;
        } else {
            TimeRemaining = 0;
            timer.set(TimeSet);
            firstSet = false;
        } 
    } else { 
        if ( TimeRemaining >= 180) {
            TimeRemaining-=180;
            timer.set(180);
            flowWakeUpFlag = false;
        } else {
            timer.set(TimeRemaining);
            TimeRemaining = 0;
            firstSet = false;
            flowWakeUpFlag = false;
        }
    }
}
void LowPower::startTime(){
    timer.startTimer();
}
void LowPower::flowWakeUp(){
   
   FlowWakeupTime = (timer.getTimeSet() - timer.getTimeElasped()) /278 ;
   int test1 = timer.getTimeSet();
   int test2 = timer.getTimeElasped();
   int test3 = test1 - test2;
   int test4 = test3/278;
   TimeRemaining += test4;
   timer.set(180);
   timer.startTimer();
}

void LowPower::flowShutDown(){
    int test2 = timer.getTimeElasped();
    int test4 = test2/278;
    int TimeLost = test4;
   
   if(TimeLost >= TimeRemaining) {
       TimeSet += (TimeLost - TimeRemaining); 
       TimeRemaining = 0;
   } else {
       TimeRemaining -= TimeLost;
   }
   flowWakeUpFlag = true;
    
}
uint16_t LowPower::getTime(){
    return TimeSet - TimeRemaining;
}
bool LowPower::CheckFlag(){
    
    if(!TimeRemaining){
        if(!timer.getTimeElasped()){
            return timer.ReturnFlag();
        }
    }
    
    
    return false;
}

    

