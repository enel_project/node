/*  Author Quinn Bancescu 
    Description: This File is responcable for running the system
*/




#include "../Interface/Interface.h"
#include "Packet.h"
#include "Sensors.h"
#include "LowPower.h"
 

bool WaitForFlow = false;
 bool TimetoSend = false;
 
     
 int main() {
     Sensor sensorData;
     LowPower sleep;
     int *sleeppointer = &sleep.TimeSet;
     Packet packet(sleeppointer);
     FlowSwitch flowSwitch;
     packet.Send(0);
     packet.Sleep(); 
     while(1){
         // *1* Check the flag to see if flow switch it triggered
         // *2* Check the flag to see if the sleep 
         // *3* If there is flow go into flow sequence
         // *4* Mask flow switch interupt
         // *5* Start timer to time flow
         // *6* Turn on ADC and Flow sensor
         // *7* Wait for there to be no more flow
         // *8* Turn off flow timer
         // *9* Turn off and read ADC and flow sensor
         // *10* If timer flag is true go into communcation sequence
         // *11* Turn on Xbee
         // *12* grab sensor readings
         // *13* Send transmission
         // *14* Turn Xbee off
         // *15* Set time on timer
         // *16* Start Timer
         // *17* go to sleep
         
         
         WaitForFlow = flowSwitch.getFlow();        // *1*
         TimetoSend = sleep.CheckFlag();            // *2*
         if(WaitForFlow != 0x00){                   // *3*         
          flowSwitch.MaskHandler();                 // *4*
          sleep.flowWakeUp();                       // *5*
          sensorData.startData();                   // *6*
          uint32_t flowTimer = 0;
          while( flowTimer < 0x186A0) {             // *7*
              if(sensorData.checkFlow() == true){
                  flowTimer = 0;
              } else {
                  flowTimer++;
              }
          }
         sleep.flowShutDown();                      // *8*
         sensorData.readData(sleep.getTime());      // *9* 
         flowSwitch.unMaskHandler();                // *10*
        }  
          if(TimetoSend != 0x00){                   // *11*
              packet.Wake();                        // *12*
              sensorData.getData(packet.sensorData);// *13*
              packet.Send(sleep.getTime());         // *14*
              packet.Sleep();                       // *15*
          }
       sleep.Settime();                             // *16*
       sleep.startTime();                           // *17*
       sleep.Sleep();                               // *18*
     }
}	

// EOF
