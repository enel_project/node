/*  Author Quinn Bancescu 
    Description: This File is responcable controling systems communication
*/

#include "Packet.h"
#define INT_MAX 65535
#define MAXBODY 36
static bool sleepflag = 0;


Packet::Packet(int *x) : xbee() {
    sleeptime = x;
    numDataPackets = 0;
    TimeoutTries = 5;
    numTransRequest = 0;
    this->TimeoutSeconds = 40000000;
    currenttime = 0;
    ConvertMACtoInt();
}

Packet::~Packet(){
}

void Packet::BuildDataPacket(){
   
   // *1* copy MAC address into packet
   // *2* increase size counter
   // *3* copy number of packets to send into packet
   // *4* increase size counter
   // *5* copy the number of data packets
   // *6* increase size counter
   // *7* set counter to last member in samples
   // *8* loop through samples 
   // *9* if there is more than 8 samples stop (8 samples is max packet size)
   // *10* filter flow 
   // *11* filter voltage
   // *12* filter temperature
   // *13* filter pressure
   // *14* remove the current member of data arrays
   // *15* rewrite the correct number of data packets being sent 
   // *16* add checksum to the packet 
   // *17* increase packet size
if(numPackets !=0){
    packet.size = 0;
    numDataPackets = 0;
    unsigned char a = 0x20;
    packet.size++;
    std::memcpy(packet.message+1, &a, 1);
    packet.size++;
    std::memcpy(packet.message+2, mac, 7);                        // *1*
    packet.size+=7;                                                        // *2*
    std::memcpy(packet.message+packet.size,&numPackets , 1);                // *3*
    packet.size++;                                                          // *4*
    std::memcpy(packet.message+packet.size,&numDataPackets , 1);            // *5*
    packet.size++;                                                          // *6*
    int i = sensorData.size-1;                                                // *7*
    for(  ;i >= 0; i-- ) {                                                   // *8*
        if(numDataPackets < 8) {                                            // *9*
            FlowFilter(sensorData.Flow[i],sensorData.Time[i]);              // *10*
            VoltageFilter(sensorData.Voltage[i], sensorData.Time[i]);       // *11*
            TemperatureFilter(sensorData.Tempature[i],sensorData.Time[i]);  // *12*
            PressureFilter(sensorData.Pressure[i],sensorData.Time[i]);      // *13*                                 
            sensorData.size--;                                              // *14*
        } else {
          break;
        }
    } 
    numPackets--;
    std::memcpy(packet.message+10,&numDataPackets , 1);                     // *15*
    std::memcpy(packet.message+packet.size,&packet.size , 1);               // *16*
    packet.size+=1;
    std::memcpy(packet.message,&packet.size , 1); 
    BuildcheckSum();
} 
}

void Packet::VoltageFilter(uint16_t sample, uint16_t time ) {
      
    // *1*  check if sample is out of bounce
    // *2*  assign type 
    // *3*  copy type to packet
    // *4*  incease size
    // *5*  copy sample to packet
    // *6*  increase size
    // *7*  copy time to packet
    // *8*  increase size
    // *9*  increment number of data packets being sent 

    unsigned char type = '0';
    if (sample < (0x7BC)){                                // *1*
        type = '6';                                                 // *2*
    } else {
        type = '2';
        } 
        std::memcpy(packet.message+packet.size, &type , 1);         // *3*
        packet.size++;;                                             // *4*
         std::memcpy(packet.message+packet.size, &sample , 2);      // *5*
        packet.size+=2;                                             // *6*
        std::memcpy(packet.message+packet.size, &time , 2);         // *7*
        packet.size+=2;                                             // *8*
        numDataPackets++;                                           // *9*
    
}
void Packet::FlowFilter(uint16_t sample, uint16_t time) {
    
    // *1*  check if sample is out of bounce
    // *2*  assign type 
    // *3*  copy type to packet
    // *4*  incease size
    // *5*  copy sample to packet
    // *6*  increase size
    // *7*  copy time to packet
    // *8*  increase size
    // *9*  increment number of data packets being sent 
    
         unsigned char type = '0';
    if (sample > (INT_MAX - 10000)){                                // *1*
        type = '5';                                                 // *2*
    } else {
        type = '1';                                                 // *2*
        } 
        std::memcpy(packet.message+packet.size, &type , 1);         // *3*
        packet.size++;;                                             // *4*
         std::memcpy(packet.message+packet.size, &sample , 2);      // *5*
        packet.size+=2;                                             // *6*
        std::memcpy(packet.message+packet.size, &time , 2);         // *7*
        packet.size+=2;                                             // *8*
        numDataPackets++;                                           // *9*
}
void Packet::TemperatureFilter(uint16_t sample, uint16_t time) {
    
    // *1*  check if sample is out of bounce
    // *2*  assign type 
    // *3*  copy type to packet
    // *4*  incease size
    // *5*  copy sample to packet
    // *6*  increase size
    // *7*  copy time to packet
    // *8*  increase size
    // *9*  increment number of data packets being sent 

    unsigned char type = '0';
   // if ((sample <= (860)) || (sample >= 960)){                                // *1*
   //     type = '7';                                                 // *2*
   // } else {
        type = '3';                                                 // *2*
     //   } 
        std::memcpy(packet.message+packet.size, &type , 1);         // *3*
        packet.size++;;                                             // *4*
         std::memcpy(packet.message+packet.size, &sample , 2);      // *5*
        packet.size+=2;                                             // *6*
        std::memcpy(packet.message+packet.size, &time , 2);         // *7*
        packet.size+=2;                                             // *8*
        numDataPackets++;                                           // *9*
}
void Packet::PressureFilter(uint16_t sample, uint16_t time) {
    
    // *1*  check if sample is out of bounce
    // *2*  assign type 
    // *3*  copy type to packet
    // *4*  incease size
    // *5*  copy sample to packet
    // *6*  increase size
    // *7*  copy time to packet
    // *8*  increase size
    // *9*  increment number of data packets being sent 


    unsigned char type = '0';
    if (sample > (INT_MAX - 10000)){                                // *1*
        type = '8';                                                 // *2*
    } else {
        type = '4';                                                 // *2*
        } 
        std::memcpy(packet.message+packet.size, &type , 1);         // *3*
        packet.size++;;                                             // *4*
         std::memcpy(packet.message+packet.size, &sample , 2);      // *5*
        packet.size+=2;                                             // *6*
        std::memcpy(packet.message+packet.size, &time , 2);         // *7*
        packet.size+=2;                                             // *8*
        numDataPackets++;                                           // *9*
}

void Packet::Send(uint16_t time){
    // *1* calculate total number of packets being sent
    // *2* round the number up packets up
    // *3* loop through bulding and sending packets 
    sleepflag = 0;
    numTransRequest = 0;
    currenttime = time;
    numPackets = (sensorData.size * 4);                     // *1*
    if(numPackets % 8 != 0 ){                               // *2*
       numPackets+=4;
        numPackets = numPackets/8;
    } else {
        numPackets = numPackets/8;
    }
    BuildRequestTransmission();
    xbee.XBeeSend(packet);
    RecieveResponce();
    CheckResponce();
   
}

void Packet::Sleep() {
    xbee.XBeeSleep();
}

void Packet::Wake() {
    xbee.XBeeWake();
    for(volatile int i = 0; i < 1000000;i++);
  //  xbee.waitForCoordinator();
}

void Packet::RecieveResponce(){
    TimeoutSeconds = 5000000;
    volatile int timeout =0;
    while(xbee.getUArtLength() !=1){
    if(timeout > TimeoutSeconds){
        gatewayResponce.size=0;
        gatewayResponce.message[0] = '0';
        return;
    }
        timeout++;
    }
    xbee.XBeeRecieve(gatewayResponce);
    uint8_t length;
    std::memcpy(&length,gatewayResponce.message,1);
    length = (int)length-1;
    if( length > 0xFF){
        for(volatile int i = 0; i < 100000;i++){
        }
        xbee.resetBuffer();
        return;
    }
    while( xbee.getUArtLength() != length){}
    xbee.XBeeRecieve(gatewayResponce);
   // xbee.resetBuffer();
}

void Packet::BuildRequestTransmission(){
    packet.size = 0;
    packet.size++;
    unsigned char a = 0x10;
    std::memcpy(packet.message+1, &a,1);
    packet.size++;
    std::memcpy(packet.message+packet.size, mac, 7);                        
    packet.size+=7;  
    std::memcpy(packet.message+packet.size,&numPackets,1);
    packet.size++;
    std::memcpy(packet.message+packet.size,&currenttime,2);
    packet.size+=2;
    std::memcpy(packet.message+packet.size,&packet.size,1);
    packet.size++;
    std::memcpy(packet.message,&packet.size,1);
    BuildcheckSum();
}
   

void Packet::RecievedAck(){
      if(numPackets !=0){                                  // *3*
        BuildDataPacket();
        xbee.XBeeSend(packet);
        RecieveResponce();
        CheckResponce();        
      }  else if (sleepflag == 0 ) {
            BuildGetRequest(); 
            xbee.XBeeSend(packet);
            RecieveResponce();
            CheckResponce();  
        }   else {
            sleepflag = 0;
        }  
}

void Packet::RecievedNack(){
      if(numTransRequest < TimeoutTries){
           numTransRequest++;
        xbee.XBeeSend(packet);
        RecieveResponce();
        CheckResponce();    
   }
      
}

void Packet::RecievedDack(){
    //shit happens in here
    int time = 0;
        unsigned char a = 0x10;
        if(std::memcmp(gatewayResponce.message+1,&a,1) == 0){
            std::memcpy(&time,gatewayResponce.message+2,2);
            *sleeptime = time;
    }
        
    
}
void Packet::CheckResponce(){ 
    unsigned char a = 0xA0;
    unsigned char b = 0xB0;
    unsigned char c = 0xC0;
    unsigned char d = 0xD0;
    if(std::memcmp(gatewayResponce.message,&a,1) == 0){
      RecievedAck();
    }
    else if(std::memcmp(gatewayResponce.message,&b,1) ==0){
        RecievedNack();
    }
    else if(std::memcmp(gatewayResponce.message,&c,1) == 0 ){
        RecievedDack();
    }
    else if(std::memcmp(gatewayResponce.message,&d,1) == 0 ){
        RecievedControl();
    }
    else {
        RecievedNack();
    }
}

void Packet::SetTimeoutTries(int numberoftries){
    TimeoutTries =numberoftries;
}
void Packet::RecievedControl(){
    *sleeptime = 0;
    int ctrLength = 0;
    uint16_t timetowake = 0;
    std::memcpy(&timetowake, gatewayResponce.message+8,2);
    *sleeptime = timetowake;
    std::memcpy(&ctrLength, gatewayResponce.message+9,1);
    uint8_t command = 0;
    for(int i=0; i < ctrLength; i++) {
        command =gatewayResponce.message[10+i];
        switch (command){
            case 0xA0:
               command =gatewayResponce.message[10+i+1];
                SetTimeoutSeconds(command);
             break;
            case 0xB0:
                command =gatewayResponce.message[10+i+1];
                SetTimeoutTries(command);
                break;
            default:
                break;
        }
        i++;
    }
    BuildEndPacket();
    xbee.XBeeSend(packet);
    RecieveResponce();
    CheckResponce();
}


void Packet::ConvertMACtoInt(){
   int a = 0;
    int index = 0;
    for (int i = 0; i < 14; i++) {
        a= 0;
        switch (xbee.MacAdress[i]) {
            case '1':
                a += (16*1);
                break;
            case '2':
                a+= (16*2);
                break;
            case '3':
                a+= (16*3);
                break;
            case '4':
                a+= (16*4);
                break;
            case '5':
                a+= (16*5);
                break;
            case'6':
                a+= (16*6);
                break;
            case'7':
                a+= (16*7);
                break;
            case '8':
                a+= (16*8);
                break;
            case '9':
                a+= (16*9);
                break;
            case 'A':
                a+= (16*10);
                break;
            case 'B' :
                a+= (16*11);
                break;
            case 'C' :
                a+= (16*12);
                break;
            case 'D':
                a+= (16*13);
                break;
            case 'E':
                a+= (16*14);
                break;
            case 'F':
                a+= (16*15);
                break;
            default:
                a+= 0;
        }
               switch (xbee.MacAdress[i+1]) {
            case '1':
                a += (1);
                break;
            case '2':
                a+= (2);
                break;
            case '3':
                a+= (3);
                break;
            case '4':
                a+= (4);
                break;
            case '5':
                a+= (5);
                break;
            case'6':
                a+= (6);
                break;
            case'7':
                a+= (7);
                break;
            case '8':
                a+= (8);
                break;
            case '9':
                a+= (9);
                break;
            case 'A':
                a+= (10);
                break;
            case 'B' :
                a+= (11);
                break;
            case 'C' :
                a+= (12);
                break;
            case 'D':
                a+= (13);
                break;
            case 'E':
                a+= (14);
                break;
            case 'F':
                a+= (15);
                break;
            default:
                a+= 0;
        }
     mac[index] = a;
     i++; 
     index++;
    }
}

void Packet::SetTimeoutSeconds(int time){
  TimeoutSeconds = time *350000 ; 
}
void Packet::BuildGetRequest(){
    unsigned char a = 0x30;
    packet.size = 0;
    packet.size++;
    std::memcpy(packet.message+packet.size,&a,1);
    packet.size++;
    std::memcpy(packet.message+packet.size,mac,7);
    packet.size+=7;
    std::memcpy(packet.message+packet.size,&packet.size,1);
    packet.size++;
    std::memcpy(packet.message,&packet.size,1);
    BuildcheckSum();
    
}

void Packet::BuildEndPacket(){
    unsigned char a = 0x40;
    packet.size = 0;
     packet.size++;
    std::memcpy(packet.message+packet.size,&a,1);
    packet.size++;
    std::memcpy(packet.message+packet.size,mac,7);
    packet.size+=7;
    std::memcpy(packet.message+packet.size,&packet.size,1);
    packet.size++;
    std::memcpy(packet.message,&packet.size,1);
    BuildcheckSum();
    sleepflag = 1;
}
void Packet::BuildcheckSum(){
    int checksum =0;
    for (int i =0; i < packet.size-1; i++){
        checksum += packet.message[i];
    }
    
    std::memcpy(packet.message+packet.size-1,&checksum,1);
}
