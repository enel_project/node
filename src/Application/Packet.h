/*  Author Quinn Bancescu 
    Description: This File is responcable controling systems communication
*/


#include "../Interface/Interface.h"
#include "LowPower.h"
#include <cstring>
#include <stdio.h>

class Packet {
    private:
        unsigned char mac[7];
        int *sleeptime ;
        uint16_t currenttime;
        int numTransRequest;
        int TimeoutTries;
        int TimeoutSeconds;
        Message gatewayResponce;
        Message packet;
        uint8_t numPackets;
        uint8_t numDataPackets;
        XBeeInterface xbee;
        void BuildDataPacket();
        void BuildRequestTransmission();
        void TemperatureFilter(uint16_t sample,uint16_t time);
        void VoltageFilter(uint16_t sample,uint16_t time);
        void PressureFilter(uint16_t sample,uint16_t time);
        void FlowFilter(uint16_t sample,uint16_t time);
        void RecievedAck();
        void RecievedNack();
        void RecievedDack();
        void CheckResponce();
        void RecieveResponce();
        void RecievedControl();
        void ConvertMACtoInt();
        void SetTimeoutSeconds(int time);
        void BuildGetRequest();
        void BuildEndPacket();
        void BuildcheckSum();
    public:
        Packet(int *x);
        ~Packet();
        void Send(uint16_t time);
        Data sensorData;
        void Sleep();
        void Wake();
        void SetTimeoutTries(int numberoftries);
    };
